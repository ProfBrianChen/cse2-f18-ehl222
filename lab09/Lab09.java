/////////
//Emma Limoges
//CSE 2 Lab 09

public class Lab09{
  public static void main (String[] args){
    int[] array0 = new int[10];
    for (int j=0; j<10; j++){
      array0[j]=j;
    }
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    inverter(array0);
    print(array0);
    inverter2(array1);
    print(array1);
    int[] array3;
    array3 = inverter2(array2);
    print(array3);
    //print(list1);
  }
  
  public static int[] copy(int[] list){
    int[] list2 = new int[list.length];
    for (int i=0; i<list.length; i++){
      list2[i] = list[i];
    }
    return list2;
  }
  
  public static void inverter(int[] array0){
    //int[] list3 = new int[list1.length];
    for (int k=0; k<(array0.length)/2; k++){
      int mover = array0[k];
      array0[k] = array0[array0.length - 1- k];
      array0[array0.length -1 - k] = mover;
    }
  }
  
  public static int[] inverter2(int[] list1){
    //int[] list4 = new int[list2.length];
    int[] list2 = copy(list1);
    for (int k=0; k<(list2.length)/2; k++){
      int mover = list2[k];
      list2[k] = list2[list2.length - 1- k];
      list2[list2.length -1 - k] = mover;
    }
    return list2;
  }
  
  public static void print(int[] array0){
    for (int m=0; m<array0.length; m++){
      System.out.print(array0[m]);
     
    }
     System.out.println("");
  }
}