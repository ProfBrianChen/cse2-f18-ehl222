////////////
//Emma Limoges
//CSE2 Lab 10

import java.util.Arrays;
public class SelectionSortLab10 {
	public static void main(String[] args) {
		int[] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9};
		int[] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1};
		int iterBest = selectionSort(myArrayBest);
		int iterWorst = selectionSort(myArrayWorst);
		System.out.println("The total number of operations performed on the sorted array: " + iterBest);
		System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);
	}

	//this method performs selection sort
	public static int selectionSort(int[] list) { 
    
		// print out the initial array.  we'll need more print statements later to see the array as it's sorted
		System.out.println(Arrays.toString(list));
		int iterations = 0; // Initialize counter for iterations

		for (int i = 0; i < (list.length - 1); i++) {
			iterations++;// Update the iterations counter
      int j;
			// Step One: Find the minimum in the list[i..list.length-1]
			int currentMin = list[i]; 
			int currentMinIndex = i;
			for (j = i +1 ; j < (list.length) ; j++) { 
				iterations ++;
        if (currentMin>list[j]){
          currentMin = list[j];
          currentMinIndex = j;
          //iterations ++;
          //System.out.println(Arrays.toString(list)); //print out our progress
        }
      }

			// Step Two: Swap list[i] with the minimum you found above
			//if (currentMinIndex != i) { 
				int temp = list[i];
        list[i] = list[currentMinIndex];
        list[currentMinIndex] = temp;
      //}
        System.out.println(Arrays.toString(list)); //print out our progress
			
      
		}
    
		return iterations;
	}
}
