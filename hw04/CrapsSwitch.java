////////////
// Emma Limoges
// CSE 2 Homework 4
// 9/23/18
// Craps Switch

//This program tells the user the slang for the dice that they roll.  The user can choose to roll the dice to randomly generate an answer or input their own data for the die values.

import java.util.Scanner; //brings in a scanner so java can read user imput

public class CrapsSwitch{
  public static void main(String args[]){ //starts the program 
    Scanner myScanner = new Scanner( System.in ); // declares the instance of the class and constructs the class
    
    int randomNumber1 = (int) (Math.random ()*6+1); //produces a random number between 1 and 6
    int randomNumber2 = (int) (Math.random ()*6+1); //produces a random number between 1 and 6
    
    System.out.print("Do you want to roll the dice? Type 'yes' or 'no' (If you choose 'no', you will have to input your own data for the die values): "); //prompts the reader to see if they want to roll the dice or not
                     String yesOrNo = myScanner.nextLine(); //takes the input and puts it into the type string
   
    
    switch (yesOrNo){
      case "yes": //the user wants randomly generated die values
        System.out.println("You rolled " + randomNumber1+ " and " + randomNumber2 + "."); //tells the user what they rolled
        
        //now we need to print out the slang
        switch (randomNumber1 + randomNumber2){
          case 2:
            System.out.println("Snake Eyes"); //prints out snake eyes if the dice add up to two
            break;
          case 3:
            System.out.println("Ace Deuce"); //prints out ace deuce if the dice add up to three
            break;
          case 4:
            if (randomNumber1==2){
              System.out.println("Hard Four"); //prints out easy four if either of the die is 2
            } 
            else {
              System.out.println("Easy Four"); //otherwise, prints out easy four
            }
            break;
          case 5:
            System.out.println("Fever Five"); //prints out fever five if the dice add up to five
            break;
          case 6:
            if (randomNumber1==3){
              System.out.println("Hard Six"); //prints out hard six if either of the die is 3
            }
            else {
              System.out.println("Easy Six"); //otherwise, prints easy six
            }
            break;
          case 7:
            System.out.println("Seven Out"); //prints out seven out if the dice add up to seven
            break;
          case 8:
            if (randomNumber1==4){
              System.out.println("Hard Eight"); //prints out hard eight if either of the die is 4
            }
            else {
              System.out.println("Easy Eight"); //otherwise, prints easy eight
            }
            break;
          case 9:
            System.out.println("Nine"); //prints out nine if the dice add up to nine
            break;
          case 10:
             if (randomNumber1==5){
              System.out.println("Hard Ten"); //prints out hard ten if either of the die is 5
            }
            else {
              System.out.println("Easy Ten"); //otherwise, prints easy ten
            }
            break;
          case 11:
            System.out.println("Yo-leven"); //prints out yo-leven if the dice add up to eleven
            break;
          case 12:
            System.out.println("Boxcars"); //prints out boxcars if the dice add up to twelve
            break;
        }
        break;
        
      case "no": //the user wants to put in their own die values
        System.out.println("Enter the number of the first die: "); //prompts the reader to give the value of the first die
        int userGivenDie1 = (int) myScanner.nextInt(); //takes the user input and puts it into the integer type
        if (userGivenDie1<1 || userGivenDie1>6){
        System.out.println("This number is invalid."); //tells the user they entered an invalid number 
        System.exit(1);
        }//only lets the user put in a value between 1 and 6
        
        System.out.println("Enter the number of the second die: "); //prompts the user to give the value of the second die
        int userGivenDie2 = (int) myScanner.nextInt(); //takes the user input and puts it into the type integer
        if (userGivenDie2<1 || userGivenDie2>6){
        System.out.println("This number is invalid."); //tells the user they entered an invalid number 
        System.exit(1);
        }//only lets the user put in a value between 1 and 6
        
        //now we need to print out the slang
        switch (userGivenDie1 + userGivenDie2){
          case 2:
            System.out.println("Snake Eyes"); //prints out snake eyes if the dice add up to two
            break;
          case 3:
            System.out.println("Ace Deuce"); //prints out ace deuce if the dice add up to three
            break;
          case 4:
            if (userGivenDie1==2){
              System.out.println("Hard Four"); //prints out easy four if either of the die is 2
            } 
            else {
              System.out.println("Easy Four"); //otherwise, prints out easy four
            }
            break;
          case 5:
            System.out.println("Fever Five"); //prints out fever five if the dice add up to five
            break;
          case 6:
            if (userGivenDie1==3){
              System.out.println("Hard Six"); //prints out hard six if either of the die is 3
            }
            else {
              System.out.println("Easy Six"); //otherwise, prints easy six
            }
            break;
          case 7:
            System.out.println("Seven Out"); //prints out seven out if the dice add up to seven
            break;
          case 8:
            if (userGivenDie1==4){
              System.out.println("Hard Eight"); //prints out hard eight if either of the die is 4
            }
            else {
              System.out.println("Easy Eight"); //otherwise, prints easy eight
            }
            break;
          case 9:
            System.out.println("Nine"); //prints out nine if the dice add up to nine
            break;
          case 10:
             if (userGivenDie1==5){
              System.out.println("Hard Ten"); //prints out hard ten if either of the die is 5
            }
            else {
              System.out.println("Easy Ten"); //otherwise, prints easy ten
            }
            break;
          case 11:
            System.out.println("Yo-leven"); //prints out yo-leven if the dice add up to eleven
            break;
          case 12:
            System.out.println("Boxcars"); //prints out boxcars if the dice add up to twelve
            break;
        }
        break;
    }
  }
}