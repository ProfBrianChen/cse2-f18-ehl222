////////////
// Emma Limoges
// CSE 2 Homework 4
// 9/23/18
// Craps If

//This program tells the user the slang for the dice that they roll.  The user can choose to roll the dice to randomly generate an answer or input their own data for the die values.

import java.util.Scanner; //brings in a scanner so java can read user imput

public class CrapsIf{
  public static void main(String args[]){ //starts the program 
    Scanner myScanner = new Scanner( System.in ); // declares the instance of the class and constructs the class
    
    int randomNumber1 = (int) (Math.random ()*6+1); //produces a random number between 1 and 6
    int randomNumber2 = (int) (Math.random ()*6+1); //produces a random number between 1 and 6
    
    System.out.print("Do you want to roll the dice? Type 'yes' or 'no' (If you choose 'no', you will have to input your own data for the die values): "); //prompts the reader to see if they want to roll the dice or not
                     String yesOrNo = myScanner.nextLine(); //takes the input and puts it into the type string
    
    //if the user wants to roll the dice
    if (yesOrNo.equals("yes")){
      System.out.println("You rolled " + randomNumber1+ " and " + randomNumber2 + "."); //tells the user what they rolled
      if (randomNumber1 ==1 && randomNumber2 ==1){
        System.out.println("Snake Eyes"); //prints snake eyes if you roll 1,1
      }
      if (randomNumber1 ==2 && randomNumber2 ==1){
        System.out.println("Ace Deuce"); //prints ace deuce if you roll 2,1
      }
      if (randomNumber1==1 && randomNumber2==2){
        System.out.println("Ace Deuce"); //prints ace deuce if you roll 1,2
      }
      if (randomNumber1==3 && randomNumber2==1){
        System.out.println("Easy Four"); //prints out easy four if you roll 3,1
      }
      if (randomNumber1==1 && randomNumber2==3){
        System.out.println("Easy Four"); //prints out easy four if you roll 1,3
      }
      if (randomNumber1==2 && randomNumber2==2){
        System.out.println("Hard Four"); //prints out hard four if you roll 2,2
      }
      if (randomNumber1==1 && randomNumber2==4){
        System.out.println("Fever Five"); //prints out fever five if you roll 1,4
      }
      if (randomNumber1==2 && randomNumber2==3){
        System.out.println("Fever Five"); //prints out fever five if you roll 2,3
      }
      if (randomNumber1==3 && randomNumber2==2){
        System.out.println("Fever Five"); //prints out fever five if you roll 3,2
      }
      if (randomNumber1==4 && randomNumber2==1){
        System.out.println("Fever Five"); //prints out fever five if you roll 4,1
      }
      if (randomNumber1==1 && randomNumber2==5){
        System.out.println("Easy Six"); //prints out easy six if you roll 1,5 
      }
      if (randomNumber1==2 && randomNumber2==4){
        System.out.println("Easy Six"); //prints out easy six if you roll 2,4
      }
      if (randomNumber1==4 && randomNumber2==2){
        System.out.println("Easy Six"); //prints out easy six if you roll 4,2 
      }
      if (randomNumber1==5 && randomNumber2==1){
        System.out.println("Easy Six"); //prints out easy six if you roll 5,1
      }
      if (randomNumber1==3 && randomNumber2==3){
        System.out.println("Hard Six"); // prints out hard six if you roll 3,3 
      }
      if (randomNumber1==1 && randomNumber2==6){
        System.out.println("Seven Out"); //prints out Seven out if you roll 1,6 
      }
      if (randomNumber1==2 && randomNumber2==5){
        System.out.println("Seven Out"); //prints out Seven out if you roll 2,5 
      }
      if (randomNumber1==3 && randomNumber2==4){
        System.out.println("Seven Out"); //prints out Seven out if you roll 3,4 
      }
      if (randomNumber1==4 && randomNumber2==3){
        System.out.println("Seven Out"); //prints out Seven out if you roll 4,3
      }
      if (randomNumber1==5 && randomNumber2==2){
        System.out.println("Seven Out"); //prints out Seven out if you roll 5,2 
      }
      if (randomNumber1==6 && randomNumber2==1){
        System.out.println("Seven Out"); //prints out Seven out if you roll 6,1
      }
      if (randomNumber1==2 && randomNumber2==6){
        System.out.println("Easy Eight"); //prints out easy eight if you roll 2,6
      }
      if (randomNumber1==3 && randomNumber2==5){
        System.out.println("Easy Eight"); //prints out easy eight if you roll 3,5 
      }
      if (randomNumber1==5 && randomNumber2==3){
        System.out.println("Easy Eight"); //prints out easy eight if you roll 5,3 
      }
      if (randomNumber1==6 && randomNumber2==2){
        System.out.println("Easy Eight"); //prints out easy eight if you roll 6,2
      }
      if (randomNumber1==4 && randomNumber2==4){
        System.out.println("Hard Eight"); //prints out hard eight if you roll 4,4 
      }
      if (randomNumber1==3 && randomNumber2==6){
        System.out.println("Nine"); //prints out nine if you roll 3,6
      }
      if (randomNumber1==4 && randomNumber2==5){
        System.out.println("Nine"); //prints out nine if you roll 4,5 
      }
      if (randomNumber1==5 && randomNumber2==4){
        System.out.println("Nine"); //prints out nine if you roll 5,4 
      }
      if (randomNumber1==6 && randomNumber2==3){
        System.out.println("Nine"); //prints out nine if you roll 6,3
      }
      if (randomNumber1==4 && randomNumber2==6){
        System.out.println("Easy Ten"); //prints out easy ten if you roll 4,6
      }
      if (randomNumber1==6 && randomNumber2==4){
        System.out.println("Easy Ten"); //prints out easy ten if you roll 6,4
      }
      if (randomNumber1==5 && randomNumber2==5){
        System.out.println("Hard Ten"); //prints out hard ten if you roll 5,5
      }
      if (randomNumber1==5 && randomNumber2==6){
        System.out.println("Yo-leven"); //prints out yo-leven if you roll 5,6 
      }
      if (randomNumber1==6 && randomNumber2==5){
        System.out.println("Yo-leven"); //prints out yo-leven if you roll 6,5 
      }
      if (randomNumber1==6 && randomNumber2==6){
        System.out.println("Boxcars"); //prints out boxcars if you roll 6,6
      }
     
    }
    
    //if the user wants to put in their own numbers
    if (yesOrNo.equals("no")){
      System.out.println("Enter the number of the first die: "); //prompts the reader to give the value of the first die
        int userGivenDie1 = (int) myScanner.nextInt(); //takes the user input and puts it into the integer type
        
      if (userGivenDie1<1 || userGivenDie1>6){
        System.out.println("This number is invalid."); //tells the user they entered an invalid number 
        System.exit(1);
      }
      
      System.out.print("Enter the number of the second die: "); //prompts the user to give the value of the second die
        int userGivenDie2 = (int) myScanner.nextInt(); //takes the user input and puts it into the type integer
        if (userGivenDie2<1 || userGivenDie2>6){
        System.out.println("This number is invalid."); //tells the user they entered an invalid number 
        System.exit(1);
        }
      
      if (userGivenDie1==1 && userGivenDie2==1){
        System.out.println("Snake Eyes"); //prints snake eyes if you roll 1,1
      }
      if (userGivenDie1 ==2 && userGivenDie2 ==1){
        System.out.println("Ace Deuce"); //prints ace deuce if you roll 2,1
      }
      if (userGivenDie1==1 && userGivenDie2==2){
        System.out.println("Ace Deuce"); //prints ace deuce if you roll 1,2
      }
      if (userGivenDie1==3 && userGivenDie2==1){
        System.out.println("Easy Four"); //prints out easy four if you roll 3,1
      }
      if (userGivenDie1==1 && userGivenDie2==3){
        System.out.println("Easy Four"); //prints out easy four if you roll 1,3
      }
      if (userGivenDie1==2 && userGivenDie2==2){
        System.out.println("Hard Four"); //prints out hard four if you roll 2,2
      }
      if (userGivenDie1==1 && userGivenDie2==4){
        System.out.println("Fever Five"); //prints out fever five if you roll 1,4
      }
      if (userGivenDie1==2 && userGivenDie2==3){
        System.out.println("Fever Five"); //prints out fever five if you roll 2,3
      }
      if (userGivenDie1==3 && userGivenDie2==2){
        System.out.println("Fever Five"); //prints out fever five if you roll 3,2
      }
      if (userGivenDie1==4 && userGivenDie2==1){
        System.out.println("Fever Five"); //prints out fever five if you roll 4,1
      }
      if (userGivenDie1==1 && userGivenDie2==5){
        System.out.println("Easy Six"); //prints out easy six if you roll 1,5 
      }
      if (userGivenDie1==2 && userGivenDie2==4){
        System.out.println("Easy Six"); //prints out easy six if you roll 2,4
      }
      if (userGivenDie1==4 && userGivenDie2==2){
        System.out.println("Easy Six"); //prints out easy six if you roll 4,2 
      }
      if (userGivenDie1==5 && userGivenDie2==1){
        System.out.println("Easy Six"); //prints out easy six if you roll 5,1
      }
      if (userGivenDie1==3 && userGivenDie2==3){
        System.out.println("Hard Six"); // prints out hard six if you roll 3,3 
      }
      if (userGivenDie1==1 && userGivenDie2==6){
        System.out.println("Seven Out"); //prints out Seven out if you roll 1,6 
      }
      if (userGivenDie1==2 && userGivenDie2==5){
        System.out.println("Seven Out"); //prints out Seven out if you roll 2,5 
      }
      if (userGivenDie1==3 && userGivenDie2==4){
        System.out.println("Seven Out"); //prints out Seven out if you roll 3,4 
      }
      if (userGivenDie1==4 && userGivenDie2==3){
        System.out.println("Seven Out"); //prints out Seven out if you roll 4,3
      }
      if (userGivenDie1==5 && userGivenDie2==2){
        System.out.println("Seven Out"); //prints out Seven out if you roll 5,2 
      }
      if (userGivenDie1==6 && userGivenDie2==1){
        System.out.println("Seven Out"); //prints out Seven out if you roll 6,1
      }
      if (userGivenDie1==2 && userGivenDie2==6){
        System.out.println("Easy Eight"); //prints out easy eight if you roll 2,6
      }
      if (userGivenDie1==3 && userGivenDie2==5){
        System.out.println("Easy Eight"); //prints out easy eight if you roll 3,5 
      }
      if (userGivenDie1==5 && userGivenDie2==3){
        System.out.println("Easy Eight"); //prints out easy eight if you roll 5,3 
      }
      if (userGivenDie1==6 && userGivenDie2==2){
        System.out.println("Easy Eight"); //prints out easy eight if you roll 6,2
      }
      if (userGivenDie1==4 && userGivenDie2==4){
        System.out.println("Hard Eight"); //prints out hard eight if you roll 4,4 
      }
      if (userGivenDie1==3 && userGivenDie2==6){
        System.out.println("Nine"); //prints out nine if you roll 3,6
      }
      if (userGivenDie1==4 && userGivenDie2==5){
        System.out.println("Nine"); //prints out nine if you roll 4,5 
      }
      if (userGivenDie1==5 && userGivenDie2==4){
        System.out.println("Nine"); //prints out nine if you roll 5,4 
      }
      if (userGivenDie1==6 && userGivenDie2==3){
        System.out.println("Nine"); //prints out nine if you roll 6,3
      }
      if (userGivenDie1==4 && userGivenDie2==6){
        System.out.println("Easy Ten"); //prints out easy ten if you roll 4,6
      }
      if (userGivenDie1==6 && userGivenDie2==4){
        System.out.println("Easy Ten"); //prints out easy ten if you roll 6,4
      }
      if (userGivenDie1==5 && userGivenDie2==5){
        System.out.println("Hard Ten"); //prints out hard ten if you roll 5,5
      }
      if (userGivenDie1==5 && userGivenDie2==6){
        System.out.println("Yo-leven"); //prints out yo-leven if you roll 5,6 
      }
      if (userGivenDie1==6 && userGivenDie2==5){
        System.out.println("Yo-leven"); //prints out yo-leven if you roll 6,5 
      }
      if (userGivenDie1==6 && userGivenDie2==6){
        System.out.println("Boxcars"); //prints out boxcars if you roll 6,6
      }
    }
   System.exit(1);   
  }
}
  