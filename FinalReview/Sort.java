import java.util.Arrays;

public class Sort{
  public static void main (String args []){
    int[] myArray = {3, 5, 2, 7, 5, 8, 29};
    insertionSort(myArray);
  }
  
  public static int[] selectionSort(int[] A){
    int maxIndex = 0;
    int max = A[0];
    for (int j=0; j<A.length-1; j++){
      max = A[j];
      maxIndex = j;
      for (int i=j+1; i<A.length; i++){
        if (A[i]>max){
          maxIndex = i;
          max = A[i];
        }
      }
      int temp = A[j];
      A[j] = A[maxIndex];
      A[maxIndex] = temp;
    }
    System.out.println(Arrays.toString(A));
    return A;
  }
  
  public static int[] insertionSort(int[] B){
    for (int i=0; i<B.length-1; i++){
      for (int j=i+1; j>0; j--){
        if (B[j]<B[j-1]){
          int temp = B[j];
          B[j] = B[j-1];
          B[j-1] = temp;
        }else{break;}
      }
    }
    System.out.println(Arrays.toString(B));
    return B;
  }
}