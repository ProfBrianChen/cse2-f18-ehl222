public class Spring14{
  public static void main (String [] args){
    
    //String ans = "This is a string.  It is full of chars.";
    for (int i=0; i<10; i++){
      for(int j=0; j<10; j++){

      System.out.println(starry(10)[i][j]);
      }
      
    }
    //System.out.println(ans.charAt(4));
    System.out.println();
  }
  
  //4
  public static char[] stringToChars(String phrase){

    boolean isValid = true;
    char[] answer = new char[phrase.length()];
    int counter = 0;
    
    for (int i=0; i<phrase.length(); i++){
      if (phrase.charAt(i) == ' ' || phrase.charAt(i) == '.'){
        answer = new char[phrase.length() - counter];
        isValid = false;
        counter ++;
      }else{isValid = true;}
      if(isValid){
        answer[i - counter] = phrase.charAt(i);
      }
    }
    return answer;
  }
  
  //5
  public static char[][] starry (int x){
    char [][] toChar = new char[x][x];
    for(int i=0; i<x; i++){
      for (int j=0; j<x; j++){
        if (j==i || j==x-i-1){
          toChar[i][j] = '*';
        }else{
          toChar[i][j] = (char)('0'+i);
        }
      }
    }
    return toChar;
  }
}