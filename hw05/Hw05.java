/////////////
//Emma Limoges
//CSE 2 Homework 5
//10/9/18

import java.util.Scanner; //import scanner

public class Hw05{
  public static void main (String args[]){ //start program
  
    Scanner myScanner = new Scanner( System.in ); //set up scanner
    
    System.out.println("How many hands would you like to generate?");
    int handsTotal = myScanner.nextInt(); //collect user input
  
    int handsSoFarFour = 0; //declare the counters
    int handsSoFarThree = 0;
    int handsSoFarTwo = 0;
    int handsSoFarOne = 0;

    int numberOfFours = 0; 
    int numberOfThrees = 0; 
    int numberOfTwos = 0;
    int numberOfOnes = 0;
  
    while(true){ //get our cards
      int card1a = (int) (Math.random ()*52+1);
      int card2a = (int) (Math.random ()*52+1); 
      int card3a = (int) (Math.random ()*52+1);
      int card4a = (int) (Math.random ()*52+1);
      int card5a = (int) (Math.random ()*52+1);

      int card1 = card1a %13;
      int card2 = card2a %13;
      int card3 = card3a %13;
      int card4 = card4a %13;
      int card5 = card5a %13;
    
      while(handsSoFarFour < handsTotal){ //determine the number of four of a kinds
        boolean four1 = (card1 == card2) && (card2 == card3) && (card3 == card4);
        boolean four2 = (card1 == card2) && (card2 == card3) && (card3 == card5);
        boolean four3 = (card1 == card2) && (card2 == card4) && (card4 == card5);
        boolean four4 = (card1 == card3) && (card3 == card4) && (card4 == card5);
        boolean four5 = (card2 == card3) && (card3 == card4) && (card4 == card5);
      
        if(four1 ^ four2 ^ four3 ^ four4 ^ four5){
          numberOfFours ++;
        }
        
        handsSoFarFour ++;
      }
    
      while(handsSoFarThree < handsTotal){ //determine the number of three of a kinds
        boolean three1 = (card1 == card2) && (card2 == card3);
        boolean three2 = (card1 == card2) && (card2 == card4);
        boolean three3 = (card1 == card2) && (card2 == card5);
        boolean three4 = (card1 == card3) && (card3 == card4);
        boolean three5 = (card1 == card3) && (card3 == card5);
        boolean three6 = (card1 == card4) && (card4 == card5);
        boolean three7 = (card2 == card3) && (card3 == card4);
        boolean three8 = (card2 == card4) && (card4 == card5);
      
        if(three1 ^ three2 ^ three3 ^ three4 ^ three5 ^ three6 ^ three7 ^ three8){
          numberOfThrees ++;
        }

        handsSoFarThree ++;
      }

      while(handsSoFarTwo < handsTotal){ //determine the number of two-pairs
        boolean two1 = (card1 == card2) && (card3 == card4);
        boolean two2 = (card1 == card2) && (card3 == card5);
        boolean two3 = (card1 == card3) && (card2 == card4);
        boolean two4 = (card1 == card3) && (card2 == card5);
        boolean two5 = (card1 == card4) && (card2 == card3);
        boolean two6 = (card1 == card4) && (card2 == card5);
        boolean two7 = (card1 == card5) && (card2 == card3);
        boolean two8 = (card1 == card5) && (card2 == card4);
        boolean two9 = (card2 == card3) && (card4 == card5);
        boolean two10 = (card2 == card4) && (card3 == card5);

        if(two1 ^ two2 ^ two3 ^ two4 ^ two5 ^ two6 ^ two7 ^ two8 ^ two9 ^ two10){
          numberOfTwos ++;
        }

        handsSoFarTwo ++;
      }
      
      while(handsSoFarOne < handsTotal){ //determine the number of one-pairs
        boolean one1 = card1 == card2;
        boolean one2 = card1 == card3;
        boolean one3 = card1 == card4;
        boolean one4 = card1 == card5;
        boolean one5 = card2 == card3;
        boolean one6 = card2 == card4;
        boolean one7 = card2 == card5;
        boolean one8 = card3 == card4;
        boolean one9 = card3 == card5;
        boolean one10 = card4 == card5;
 
       if(one1 ^ one2 ^ one3 ^ one4 ^ one5 ^ one6 ^ one7 ^ one8 ^ one9 ^ one10){
         numberOfOnes ++;
       }

      handsSoFarOne ++;
    }

      if ((card1a != card2a) && (card2a != card3a) && (card3a != card4a) && (card4a != card5a)){ //stop code if two cards are the same
        break;
      }
    }

    //print out the results
    System.out.println("The number of loops: " + handsTotal);
    System.out.println("The probability of Four-of-a-kind: " + (numberOfFours/handsTotal));
    System.out.println("The probability of Three-of-a-kind: " + (numberOfThrees/handsTotal));
    System.out.println("The probability of Two-pair: " + (numberOfTwos/handsTotal));
    System.out.println("The probability of One-pair: " + (numberOfOnes/handsTotal));
  }
}





