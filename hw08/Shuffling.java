/////////////
//Emma Limoges
//CSE2 HW08

import java.util.Scanner;
public class Shuffling{ 

  public static void main(String[] args) { 
    Scanner scan = new Scanner(System.in); 
    //suits club, heart, spade or diamond 
    String[] suitNames={"C", "H", "S", "D"};    
    String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"}; 
    String[] cards = new String[52]; 
    String[] hand = new String[5]; 
    int numCards = 0; 
    int again = 1; 
    int index = 51;

    printArray(cards, rankNames, suitNames); 
    System.out.println("");
    System.out.println("Shuffled");
    shuffle(cards); 
    System.out.println("");
    //printArray(cards); 
    
    System.out.println("How many cards would you like to draw?");
    numCards = scan.nextInt();
    
    if (numCards > 52){
      System.out.println("You have created another deck of cards");
      printArray(cards, rankNames, suitNames);
      System.out.println("Shuffled");
      shuffle(cards); 
      System.out.println("");
    }
    
    while(again == 1){ 
      //hand = getHand(cards, index, numCards); 
      getHand(cards, index, numCards);
      //printArray(hand, rankNames, suitNames);
      index = index - numCards;
      System.out.println("");
      System.out.println("Enter a 1 if you want another hand drawn.  Enter any other key to escape."); 
      again = scan.nextInt(); 
    }  
    
    System.out.println("");
    
  } 
  
  public static void printArray(String[] cards, String[] rankNames, String[] suitNames){
    for (int i=0; i<52; i++){ 
      cards[i]=rankNames[i%13]+suitNames[i/13]; 
      System.out.print(cards[i]+" "); 
    }
  }
  
  public static void shuffle(String[] cards){
    for (int j=0; j<52; j++){
      double cardSpot1 = (cards.length * Math.random());
      int cardSpotInt1 = (int) cardSpot1;
      String cardSpotString1 = Double.toString(cardSpot1);
      String cardSpot2 = cards[cardSpotInt1];
      cards[cardSpotInt1] = cards[j];
      cards[j] = cardSpot2;
      System.out.print(cards[j] + " ");
    };
  }
  
  public static void getHand(String[] cards, int index, int numCards){
    double indexDouble = index;
    String indexString = Double.toString(indexDouble);
    for (int k=0; k<numCards; k++){
      System.out.print(cards[index-k] + " ");
    }
  }

}
