//////////
//Emma Limoges
// CSE2 HW9

import java.util.Scanner;
public class RemoveElements{
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
  int num[]=new int[10];
  int newArray1[];
  int newArray2[];
  int index,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
    System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  
  }
 
  public static String listArray(int num[]){
    String out="{";
    for(int j=0;j<num.length;j++){
      if(j>0){
        out+=", ";
      }
      out+=num[j];
    }
    out+="} ";
    return out;
  }
    
  public static int[] randomInput(){
    int myArray[] = new int[10]; //create an array of length 10
    int k;
    for (k=0; k<10; k++){
      myArray[k] = (int) (Math.random()*10); //fills the array with random integers between 0 and 9
    }
    return myArray;
  }
  
  public static int[] delete(int[] list, int pos){
    if (pos<0 || pos>list.length){
      System.out.println("The index is not valid.");
    }
    int newList[] = new int[list.length -1]; //makes the new array one index shorter
    for (int i=0; i<pos; i++){ //gives the same values up until the given index
      newList[i] = list[i];
    }
    for (int j=pos; j<newList.length; j++){ //gives the values of the next position after the given index
      newList[j] = list[j+1];
    }
    return newList;    
  }
    
  public static int[] remove(int[] list, int target){
    
    int newList2[] = new int[list.length-2]; //we need a new pointer
    int m;
    int n = 0;
    int amountRemoved = 0;
    
    for (m=0; m<newList2.length; m++){ //this will remove the values
      if (list[m] == target){
        //n++;
        amountRemoved++;
      }
      if(m+amountRemoved<newList2.length){
        newList2[m] = list[m+amountRemoved];
       // n++;
      }else{ //set the extra members = 0
        newList2[m] = 0;
        //n++;
      }
    }
    
    int result[] = new int[newList2.length-amountRemoved]; //create a new pointer
    
    for (int p=0; p<newList2.length-amountRemoved; p++){ //shorten the array
      result[p] = newList2[p];
    }
    return result;
  }
   
}