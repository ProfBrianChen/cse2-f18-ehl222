//////////////
// Emma Limoges
// 9/9/18
// CSE 2 Homework 02
//
public class Arithmetic {
  //starts a Java program
  public static void main (String[] args) {
    //Declare input variables / assumptions
//Number of pairs of pants
int numPants=3;
//Cost per pair of pants
double pantsPrice=34.98;
//Number of sweatshirts
int numShirts=2;
//Cost per sweatshirt
double shirtPrice=24.99;
//Number of belts
int numBelts=1;
//cost per belt
double beltCost=33.99;
//the tax rate
double paSalesTax=0.06;

    //Computations
    //Assign output variables
    double totalCostOfPants=numPants*pantsPrice;   //total cost of pants is Number of pairs of pants times cost per pair of pantsPrice
    double totalCostOfShirts=numShirts*shirtPrice;   //total cost of sweatshirts is Number of sweatshirts times cost per sweatshirt
    double totalCostOfBelts=numBelts*beltCost;   //total cost of belts is Number of belts times cost per beltCost
    
    double salesTaxPants=totalCostOfPants*paSalesTax;   //sales tax on pants
    double salesTaxShirts=totalCostOfShirts*paSalesTax;  //sales tax on sweatshirts
    double salesTaxBelts=totalCostOfBelts*paSalesTax;  //sales tax on belts
    
    double totalCostNoTax=totalCostOfPants+totalCostOfShirts+totalCostOfBelts;  //total cost without sales tax
    double totalTax=totalCostNoTax*paSalesTax; //gives all the tax that needs to be paid on this purchase
    
    //get rid of partial pennies in my costs
    double convertingSalesTaxPants=salesTaxPants*100;
    int intSalesTaxPants= (int) convertingSalesTaxPants;
      double doubleSalesTaxPants= (double) intSalesTaxPants/100;  //these three lines make the sales tax for pants accurate to .01 dollars.  This is truncated and not rounded.
    double convertingSalesTaxShirts=salesTaxShirts*100;
    int intSalesTaxShirts= (int) convertingSalesTaxShirts;
      double doubleSalesTaxShirts= (double) intSalesTaxShirts/100; //these three lines make the sales tax for shirts accurate to .01 dollars.  This is truncated and not rounded.
    double convertingSalesTaxBelts=salesTaxBelts*100;
    int intSalesTaxBelts= (int) convertingSalesTaxBelts;
      double doubleSalesTaxBelts= (double) intSalesTaxBelts/100; //these three lines make the sales tax for belts accurate to .01 dollars.  This is truncated and not rounded.
    double convertingTotalTax=totalTax*100;
    int intTotalTax= (int) convertingTotalTax;
    double doubleTotalTax= (double) intTotalTax/100; //these three lines make the total sales tax accurate to .01 dollars.  This is truncated and not rounded.
        
    double totalCostWithTax=totalCostNoTax+doubleTotalTax;  //gives us the total paid on this purchase with tax
    
    //tell Java to print out the desired vales
    System.out.println(" Total cost of pants is $"+totalCostOfPants); //prints total cost of pants
    System.out.println(" Total cost of sweatshirts is $"+totalCostOfShirts); //prints total cost of sweatshirts
    System.out.println(" Total cost of belts is $"+totalCostOfBelts);  //prints total cost of belts
    
    System.out.println(" Tax on pants is $"+doubleSalesTaxPants); //prints tax on pants 
    System.out.println(" Tax on sweatshirts is $"+doubleSalesTaxShirts); //prints tax on shirts 
    System.out.println(" Tax on belts is $"+doubleSalesTaxBelts); //prints tax on belts 
    
    System.out.println(" Total cost without tax is $"+totalCostNoTax); //prints the total before tax is added
    System.out.println(" Total sales tax is $"+doubleTotalTax); //prints the total sales tax
    
    System.out.println(" Total paid in this transaction including sales tax is $"+totalCostWithTax); //prints the total cost for this transaction including sales tax
    
  }
  
} //end the program