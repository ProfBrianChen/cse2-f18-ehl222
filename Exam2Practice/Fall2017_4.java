//FINAL EXAM
//you don't need to use the scanner bc you're given input n???

public class Fall2017_4{
  
  public static void main(String args[]){
    
    int inputn = 8;
    int n = inputn;
    if (inputn < 0 ){n = -inputn;} //this isn't working for negative values yet, or values between -6 and 6
    
    for( int i = 0; i<n; i++){
      for ( int j = 0; j<n; j++){
        if(i ==j || i==j-1 || i==j+1){
          if(n > 0){
            System.out.print("#");
          }else{
          System.out.print(".");
          }
        }
        else if (j == ((n-1)-i) || j == n-i || j == n-2-i){
          if (n > 0){
          System.out.print("#");
          }else {
            System.out.print(".");
          }
        }
        else {System.out.print(".");}
        
      }
      System.out.print("\n");
    }
  }
}
