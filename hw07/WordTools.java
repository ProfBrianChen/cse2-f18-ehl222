//////
//Emma Limoges
//CSE 2 Homework 7

import java.util.Scanner; //we need this to read user input

public class WordTools{
  
  public static void main (String [] args){ //this is the main method
    
    Scanner scanner3 = new Scanner( System.in ); //construct the scanner class
    //System.out.println("You entered: " + sampleText() );
    //System.out.println("You chose: " + printMenu() );
    switch (printMenu()){ //depending on what the user enters in printMenu, the main method will run one of the following methods
      case 'c':
      System.out.println(getNumOfNonWSCharacters(sampleText()) - getNumOfWords(sampleText()));
      break;
      case 'w':
      System.out.println(getNumOfWords(sampleText()));
      break;
      case 'f':
      System.out.println("What word do you want to find?");
      String wordToFind = scanner3.next();
      break;
      case 'r':
      System.out.println(replaceExclamation(sampleText()));
      break;
      case 's':
      System.out.println(shortenSpace(sampleText()));
      break;
      case 'q':
      break;
      default: 
      break;
    }
  }
  
  public static String sampleText(){ //this method gets the sampe text from the user
  
    Scanner scanner1 = new Scanner( System.in );
    
    System.out.println("Enter a sample text:");
    
    String userText = scanner1.nextLine();
    
    System.out.println("You entered: " + userText);
    
    return userText;
    
  }
  
  public static char printMenu(){ //this method offers the user choices of what to do with the sample text
    
    Scanner scanner2 = new Scanner( System.in );
    
    //boolean getChar = false;
    
    //while (getChar == false){
    
    System.out.println("MENU");
    System.out.println("");
    System.out.println("c - number of non white-space characters");
    System.out.println("w - number of words");
    System.out.println("f - find text");
    System.out.println("r - replace all !'s");
    System.out.println("s - shorten spaces");
    System.out.println("q - quit");
    System.out.println("");
    System.out.println("Choose an option:");
    
    //boolean getChar = scanner2.hasNext(); //FIXME - getChar is already defined; has next char is not a command
    
    //}
    
    char menuChoice = scanner2.next().charAt(0);  
    
    System.out.println("You chose: " + menuChoice );
    
    return menuChoice;
    
  }
  
  public static int getNumOfNonWSCharacters(String userText){ //this method finds the number of characters that are not spaces
    
    int numChar = userText.length(); 
    
    /*int charCount = 0;
    char temp;

    for( int i = 0; i < numChar; i++ ){
      
      temp = userText.charAt(' '); //this does not make sense

      if( temp == 0) {charCount++;}
      
    }
    int nonWS = numChar - charCount; */
    return numChar;
  }
  
  public static int getNumOfWords(String userText){ //this method finds the numbre of words
    int numChar = userText.length();
    int i = 0;
    int j = 0;
    if (userText.charAt(i) == 0){j++;}
    return j;
  }
  
  public static int findText(String userText, String wordToFind){ //this method finds a specific word in the sample text
    int instances = 0;
    return instances;
  }
  
  public static char replaceExclamation(String userText){ //this method replaces exclamtions with periods
    char exclamation = '.';
    return exclamation;
  }
  
  public static int shortenSpace(String userText){ //this method shortens the spaces when there are two or more in a row
    char spaces = ' ';
    return spaces;
  }
}