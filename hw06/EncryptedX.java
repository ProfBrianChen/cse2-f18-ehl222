/////////
//Emma Limoges
//CSE 2 Homework 6
//10/21/18

//This program will use nested loops to create a pattern that reveals an X in the spaces between *s

import java.util.Scanner; //we need this to read user inputs

public class EncryptedX{ //start the class
  public static void main (String [] args){ //declare the method
    
    Scanner myScanner = new Scanner( System.in ); //construct the scanner method
    
    //declare these variables outside the scope of the loops
    boolean getInt = false;  
    String flush;
    int squareSize;
    
    //use the scanner to get user input
    while (getInt == false){
      System.out.println ("How big would you like your pattern?  Enter an integer between 1 and 100 for the number of rows and columns.");
      
      getInt = myScanner.hasNextInt(); 
      //squareSize = myScanner.nextInt();
      
      if((!getInt)){ // if the user doesn't enter an integer between 1 and 100
        System.out.println("Error.  Please enter an integer between 1 and 100.");
        flush = myScanner.next(); //this is what flushes the scanner so i can use it again
      }
      else{
        squareSize = myScanner.nextInt(); //if the user does enter an integer
        /*if (squareSize<1 || squareSize>100){
          System.out.println("Error.  Please enter an integer between 1 and 100.");
          flush = myScanner.next(); //this is what flushes the scanner so i can use it again
        }
        else{ 
          squareSize = myScanner.nextInt();//if the user does enter an integer*/

    
    
    //start making the pattern
    int i;
    int j;
    int k;
    int l;
    
    for (i = 0; i < squareSize; ++i){
      for (j = 0; j < squareSize; ++j){
        if ( i == j || j == squareSize - 1 - i){
          System.out.print(" ");
        }
        else{
          System.out.print("*");
        }
      }
      System.out.println("");
    }
      }
    }
    
  }
    
}