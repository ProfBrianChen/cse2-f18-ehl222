////////////////
//Emma Limoges
//9/16/18
//CSE 2 HW03 Pyramid

import java.util.Scanner; //imports the scanner so we can use it
public class Pyramid{
   
  public static void main( String [] args){ //starts program
    Scanner myScanner = new Scanner( System.in ); //declares an instance of the class, then uses the construction operator new to construst the class
    //get user input to use for caluclations
    System.out.print("The square side of the pyramid is (input length): "); //prompts the user to put in the length
    double length = myScanner.nextDouble(); //turns the input into a variable java can use in calculations
    System.out.print("The height of the pyramid is (input height): "); //prompts the user to put in the height
    double height = myScanner.nextDouble(); //turns the input into a variable java can use in calculations
    
    //tell java to perform the calculations
    double base = length*length; //calulates the base of the pyramid
    double volumeTimes3 = base*height;
    int volume = (int) (volumeTimes3/3); //calulates the volume
    
    //print out the result
    System.out.println("The volume inside the pyramid is: "+volume+"."); //java will print out the result
  }
}//ends the program