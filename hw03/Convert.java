////////////////
//Emma Limoges
//9/16/18
//CSE 2 HW03 Convert

import java.util.Scanner; //imports the scanner so we can use it
public class Convert{
   
  public static void main( String [] args){ //starts program
    Scanner myScanner = new Scanner( System.in ); //declares an instance of the class, then uses the construction operator new to construst the class
    //get user input to use for caluclations
    System.out.print("Enter the area affected in acres: "); //java will prompt the user to enter the are affected
    double area = myScanner.nextDouble(); //turns the user input into a variable java can use
    System.out.print("Enter the rainfall in the affected area in inches: "); //java will prompt the user to enter the amount of rainfall
    double inches = myScanner.nextDouble(); //turns the user input into a variable we can user
    
    //tell java to perform the calculations
    double gallons = area*inches*27154; //gives the amount of rainfall in gallons.  I used USGS rainfall calculator. 
    double cubicMiles = gallons*9.08169e-13; //gives the amount of rainfall in cubic miles.  I used Google's unit conversion tool
    
    //put it with the right number of decimal places
    double cubicMilesTimesALot = (int) (cubicMiles*10e7);
    double cubicMilesFinal = (cubicMilesTimesALot/10e7);
    
    //tell java to print the answer
    System.out.println(cubicMilesFinal +" cubic miles"); //tells java to print the answer
    
  }
} //ends the program