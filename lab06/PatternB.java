/////////
//Emma Limoges
//10/11/18
//CSE 2 Lab 6

//This code will use loops to create a pattern

import java.util.Scanner; //we need this to read user data

public class PatternB{
  public static void main (String args[]){ //start program
    
    Scanner myScanner = new Scanner( System.in ); //set up the scanner
    
    int totalLines; //we need to declare these variables outside of the scope
    boolean getInt = false;
    int numLines = 0;
    String flush;
    
    //use the scanner to get user input
    while (getInt == false){
      System.out.println ("How many rows do you want in your pattern?");
      
      getInt = myScanner.hasNextInt(); 
      
      
      if(!getInt){
        System.out.println("Error.  Please enter an integer.");
        flush = myScanner.next(); //this is what flushes the scanner so i can use it again
      }
      else numLines = myScanner.nextInt();
    }
       
    //make the pattern now! 
    int j;
    int i;
        
    for (j=numLines; j >= 1; --j){
      for (i=1; i <= j; ++i){
        System.out.print (i + " "); 
      }
      System.out.println("");
    }
        
  }
}