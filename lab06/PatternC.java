/////////
//Emma Limoges
//10/11/18
//CSE 2 Lab 6

//This code will use loops to create a pattern

import java.util.Scanner; //we need this to read user data

public class PatternC{
  public static void main (String args[]){ //start program
    
    Scanner myScanner = new Scanner( System.in ); //set up the scanner
    
    int totalLines; //we need to declare these variables outside of the scope
    boolean getInt = false;
    int numLines = 0;
    String flush;
    
    //use the scanner to get user input
    while (getInt == false){
      System.out.println ("How many rows do you want in your pattern?");
      
      getInt = myScanner.hasNextInt(); 
      
      if(!getInt){
        System.out.println("Error.  Please enter an integer.");
        flush = myScanner.next(); //this is what flushes the scanner so i can use it again
      }
      else numLines = myScanner.nextInt();
    }

    //write the pattern now
    int i;
    int j = numLines;
    int k;
       
    for (k=1; k <= numLines; ++k){
      for (i=numLines; i >= k; --i){
        System.out.print(" ");
      }
      for (j=k; j > 0; --j){
        System.out.print (j); 
      }
      System.out.println("");
    }
 
  }
}