public class Fall2013{
  public static void main(String[] args){
    int[] myArray = {2, 1, 2, -2, 4};
    int min = 0;
    int max = 4;
    for (int i=0; i<swapMember(myArray, min, max).length; i++){
      System.out.print(sort(myArray)[i]+ " ");
    }
    System.out.println();
  }
  
  public static int findMinInRange(int[] A, int min, int max){
    int smallest = min;
    if( min>max || max>A.length){
      System.out.println("Unacceptable input");
      return -1;
    }else{
      for (int i=min; i<max; i++){
        if (A[i+1]<A[i]){
          smallest = i+1;
        }
      }
      return smallest;
    }
  }
    
  public static int[] swapMember(int[] A, int index1, int index2){
    if (index1>A.length-1 || index2>A.length-1){
      System.out.println("Unacceptable input.");
      return A;
    }else if (index1<0 || index2<0){
      System.out.println("Unacceptable input.");
      return A;
    }else{
      int[] newArray = new int[A.length];
      for (int i=0; i<A.length; i++){
        newArray[i] = A[i];
      }
      int temp = newArray[index1];
      newArray[index1] = newArray[index2];
      newArray[index2] = temp;
      return newArray;
    }      
  }
  public static int[] sort(int[] A){
    int[] B = new int[A.length];
    for (int j=0; j<A.length; j++){
      B[j] = A[j];
    }
    for (int i=0; i<A.length; i++){
      int index1 = findMinInRange(B, i, B.length-1);
      B = swapMember(B, index1, i);
    }
    return B;
  }
  
}