public class Fall2014{
  public static void main(String[] args){
    
    int[] myArray = {1, 2, 3, 4, 5};
    
    //System.out.println(search(myArray, 4, 0, 2));
    for (int i=0; i<(noReps(myArray).length); i++){
      System.out.print(noReps(myArray)[i]);
    }
  }
    public static boolean search(int[] myArray, int target, int minRange, int maxRange){
      
      boolean answer = false;
      int i;
      
      for (i=minRange; i<=maxRange; i++){
        if (myArray[i]==target){
          answer = true;
        }
      }
      
      return answer;
    }
  
  public static int[] resize(int[] myArray, int newSize){
    int[] newArray = new int[newSize];
    if (newSize<(myArray.length)){
      for (int m=0; m<newSize; m++){
        newArray[m] = myArray[m];
        System.out.print(newArray[m] + " ");
      }
    }
    if (newSize>myArray.length){
      for (int i=0; i<myArray.length; i++){
        newArray[i] = myArray[i];
        System.out.print(newArray[i] + " ");
      }
      for (int j=0; j<newSize; j++){
        newArray[j] = 0;
        System.out.print(newArray + " ");
      }
    }
    if (newSize == (myArray.length)){
      for (int k=0; k<myArray.length; k++){
        newArray[k] = myArray[k];
        System.out.print(newArray[k] + " ");
      }
    }
    System.out.println();
    return newArray;
  }
  
  public static int[] noReps(int[] myArray){
    int[] result = new int[myArray.length];
    int j = 0;
    for (int i=0; i<myArray.length; i++){
      boolean test = search (myArray, myArray[i], 0, myArray.length-1);
      if (test == false){
        result[i] = myArray[i-j];
      }
      if (test == true){
        j++;
      }
    }
    result = resize(result, (myArray.length-j));
    return result;
  }
}