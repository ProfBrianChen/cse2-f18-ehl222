///////////
//Emma Limoges
//CSE2 HW 9

//This method will search fo grades given by user inputs

import java.util.Scanner; //we need this to get user inputs
import java.util.Random; //this will randomize stuff

public class CSE2Linear{ //declare the class
  public static void main (String[] args){ //declare the main method
    
    Scanner myScanner = new Scanner( System.in ); //set up the scanner to use in this method
    int i = 0;
    int m = 0;
    String flush;
    boolean error;//these variables need to be declared outside of the scopes
    int [] grades = new int[16]; //declare and allocate my array
    
    //now we collect the grades using user inputs
    System.out.println("Enter 15 ascending ints for final grades in CSE2:");
    while(i<grades.length -1){
      error = myScanner.hasNextInt();     
      if (error == false){
        System.out.println("Error.  Please enter an integer.");
        flush = myScanner.next();
      }     
      else {
        grades[i+1] = myScanner.nextInt(); //set this to be the member of the array in the ith position       
        if (grades[i+1]<0 || grades[i+1]>100){
          System.out.println("Error.  Please enter a number less than or equal to 100.");
          flush = myScanner.next();
          i++;
        }
        else if (grades[i+1]<grades[i]){
          System.out.println("Error.  Please enter a number greater than or equal to the last number.");
          flush = myScanner.next();
          i++;
        }
        else {
          i++;
        } //move to the next index in the array
      }     
    }
    
    binarySearch(grades); //call the method to run
    System.out.println("Scrambled:");
    //print out the randomized array
    for (m=0; m<(randomize(grades)).length; m++){
      System.out.print(((randomize(grades))[m]) + " ");
    }
    System.out.println(" ");
    linearSearch(randomize(grades)); //call on the method to run
    
  }
    
  //now we start the search
  public static void binarySearch(int[] grades){ //FIXED

    Scanner myScanner = new Scanner( System.in ); //set up the scanner to use in this method
    int lookFor;
    System.out.println("Enter a grade to search for:");
    lookFor = myScanner.nextInt();
    int low = 0;
    int high = grades.length -2;
    int j = 0;
    while (high>=low){
      j++;
      int mid = (low+high)/2;
      if (grades[mid] < lookFor){
        low = mid+1;
      }
      if (grades[mid] > lookFor){
        high = mid-1;
      }
      if (grades[mid] == lookFor){
        System.out.println(lookFor + " was found after " + (j) + " iterations.");
        break;
      }
    }
    if (high<low){
      System.out.println(lookFor + " was not found after " + (j) + " iterations.");
    }

  }

  //this method will shuffle the contents of the array
  public static int[] randomize(int[] grades){ 
    
    int [] randomArray = new int[grades.length-2];
    for (int p=0; p<grades.length-2; p++){
      randomArray[p] = grades[p];
    }
    
    Random ran = new Random(10);
    int randomInt;
    while (true){
      randomInt = ran.nextInt();
      if (randomInt<randomArray.length && randomInt>0){
        break;
      }
    } //set up Random to use - this takes a long time
    
    int k;
    for(k=0; k<randomArray.length; k++){
      int target = randomInt;
      int temp = randomArray[target];
      randomArray[target] = randomArray[k];
      randomArray[k] = temp;
    } //this for loop came from lecture 18

  return randomArray;

  }

  //now we start the linear search
  public static void linearSearch(int[] grades){ //FIXED
    
    Scanner myScanner = new Scanner( System.in ); //set up the scanner to use in this method
    int lookFor;
    System.out.println("Enter a grade to search for:");
    lookFor = myScanner.nextInt();
    
    int n = 0;
    for (n=0; n<grades.length-2; n++){
      if (grades[n] == lookFor){
        System.out.println(lookFor + " was found after " + (n+1) + " iterations.");
        break;
      }
    }
    if (grades[n] != lookFor){
      System.out.println(lookFor + " was not found after " + (n+1) + " iterations.");
    }
    
  }
  
}