////////////
//Emma Limoges
//10/25/18
//CSE 2 Lab 7

import java.util.Random; //we need to use the random number generator

public class Lab07{ //declare class
  public static void main (String [] args){ //declare main method
    
    Random randomGenerator = new Random(); //create the random object
    int randomInt1 = randomGenerator.nextInt(10); //declare randomInt, which will give us random integers less than 10
    int randomInt2 = randomGenerator.nextInt(10);
    int randomInt3 = randomGenerator.nextInt(10);
    int randomInt4 = randomGenerator.nextInt(10);
    int randomInt5 = randomGenerator.nextInt(10);
    int randomInt6 = randomGenerator.nextInt(10);
    int randomInt8 = randomGenerator.nextInt(10);
    
    String adjective1 = Adjective1(randomInt1);
    String adjective2 = Adjective2(randomInt5);
    String adjective3 = Adjective3(randomInt6);
    String subject = Subject(randomInt2);
    String verb = Verb(randomInt3);
    String object = Objectt(randomInt4);
    String verb2 = Verb2(randomInt8);
    
    System.out.println("The " + adjective1 + adjective2 + subject + verb2 + "the " + adjective3 + object + ".");
    main2(subject, object);
  }
  
   public static void main2 (String subject, String object){ //declare main method
    
    Random randomGenerator = new Random(); //create the random object
    int randomInt1 = randomGenerator.nextInt(10); //declare randomInt, which will give us random integers less than 10
    int randomInt2 = randomGenerator.nextInt(10);
    int randomInt3 = randomGenerator.nextInt(10);
    int randomInt4 = randomGenerator.nextInt(10);
    int randomInt5 = randomGenerator.nextInt(10);
    int randomInt6 = randomGenerator.nextInt(10);
    int randomInt7 = randomGenerator.nextInt(10);
    int randomInt8 = randomGenerator.nextInt(10);
    int randomInt9 = randomGenerator.nextInt(10);
    
    String adjective1 = Adjective1(randomInt1);
    String adjective2 = Adjective2(randomInt5);
    String adverb = Adjective3(randomInt6);
    String object3 = Objectt2(randomInt7);
    String verb = Verb(randomInt3);
    String object2 = Objectt(randomInt4);
    String verb2 = Verb2(randomInt8);
    String object4 = Objectt4(randomInt9);
     
    System.out.println("This " + subject + "was " + adjective2 + "to the " + object + ".");
    System.out.println("It used the " + object2 + " to " + verb + object3 + "at the " + adjective1 + object + ".");
    System.out.println("That " + subject + verb2 + "its " + object4 +"!");
  }
    
    public static String Adjective1 (int randomInt1){ //this will generate a random adjective from the ones that I listed
      String adj = "";
      switch (randomInt1){
        case 1:
          adj = "pretty ";
          break;
        case 2:
          adj = "funny ";
          break;
        case 3:
          adj = "hairy ";
          break;
        case 4:
          adj = "sweet ";
          break;
        case 5:
          adj = "squishy ";
          break;
        case 6:
          adj = "clever ";
          break;
        case 7:
          adj = "angry ";
          break;
        case 8:
          adj = "cold ";
          break;
        case 9:
          adj = "lumpy ";
          break;
        case 0:
          adj = "bony ";
      }
      
      return adj;
      
    }
  
  public static String Subject (int randomInt2){ //this will generate a random adjective from the ones that I listed
      String sub = "";
      switch (randomInt2){
        case 1:
          sub = "penguin ";
          break;
        case 2:
          sub = "kid ";
          break;
        case 3:
          sub = "sandal ";
          break;
        case 4:
          sub = "cake ";
          break;
        case 5:
          sub = "thermometer ";
          break;
        case 6:
          sub = "juice ";
          break;
        case 7:
          sub = "clarinet ";
          break;
        case 8:
          sub = "acorn ";
          break;
        case 9:
          sub = "raft ";
          break;
        case 0:
          sub = "sponge ";
      }
      
      return sub;
      
    }
  
  public static String Verb (int randomInt3){ //this will generate a random adjective from the ones that I listed
      String vb = "";
      switch (randomInt3){
        case 1:
          vb = "give away ";
          break;
        case 2:
          vb = "write about ";
          break;
        case 3:
          vb = "dance with ";
          break;
        case 4:
          vb = "climb up ";
          break;
        case 5:
          vb = "wash ";
          break;
        case 6:
          vb = "make ";
          break;
        case 7:
          vb = "wear ";
          break;
        case 8:
          vb = "watch ";
          break;
        case 9:
          vb = "jump over ";
          break;
        case 0:
          vb = "eat ";
      }
      
      return vb;
      
    }
  public static String Objectt (int randomInt4){ //this will generate a random adjective from the ones that I listed
      String ob = "";
      switch (randomInt4){
        case 1:
          ob = "candy";
          break;
        case 2:
          ob = "bottle";
          break;
        case 3:
          ob = "keys";
          break;
        case 4:
          ob = "mop";
          break;
        case 5:
          ob = "apples";
          break;
        case 6:
          ob = "toothbrush";
          break;
        case 7:
          ob = "grass";
          break;
        case 8:
          ob = "scissors";
          break;
        case 9:
          ob = "moon";
          break;
        case 0:
          ob = "sweater";
      }
      
      return ob;
      
    }
     public static String Adjective2 (int randomInt5){ //this will generate a random adjective from the ones that I listed
      String adj = "";
      switch (randomInt5){
        case 1:
          adj = "pretty ";
          break;
        case 2:
          adj = "funny ";
          break;
        case 3:
          adj = "hairy ";
          break;
        case 4:
          adj = "sweet ";
          break;
        case 5:
          adj = "squishy ";
          break;
        case 6:
          adj = "clever ";
          break;
        case 7:
          adj = "angry ";
          break;
        case 8:
          adj = "cold ";
          break;
        case 9:
          adj = "lumpy ";
          break;
        case 0:
          adj = "bony ";
      }
      
      return adj;
      
    }
  
     public static String Adjective3 (int randomInt6){ //this will generate a random adjective from the ones that I listed
      String adj = "";
      switch (randomInt6){
        case 1:
          adj = "pretty ";
          break;
        case 2:
          adj = "funny ";
          break;
        case 3:
          adj = "hairy ";
          break;
        case 4:
          adj = "sweet ";
          break;
        case 5:
          adj = "squishy ";
          break;
        case 6:
          adj = "clever ";
          break;
        case 7:
          adj = "angry ";
          break;
        case 8:
          adj = "cold ";
          break;
        case 9:
          adj = "lumpy ";
          break;
        case 0:
          adj = "bony ";
      }
      
      return adj;
      
    }
    public static String Objectt2 (int randomInt7){ //this will generate a random adjective from the ones that I listed
      String ob = "";
      switch (randomInt7){
        case 1:
          ob = "candy ";
          break;
        case 2:
          ob = "bottle ";
          break;
        case 3:
          ob = "keys ";
          break;
        case 4:
          ob = "mop ";
          break;
        case 5:
          ob = "apples ";
          break;
        case 6:
          ob = "toothbrush ";
          break;
        case 7:
          ob = "grass ";
          break;
        case 8:
          ob = "scissors ";
          break;
        case 9:
          ob = "moon ";
          break;
        case 0:
          ob = "sweater ";
      }
      
      return ob;
      
    }
  public static String Verb2 (int randomInt8){ //this will generate a random adjective from the ones that I listed
      String vb = "";
      switch (randomInt8){
        case 1:
          vb = "gave away ";
          break;
        case 2:
          vb = "wrote about ";
          break;
        case 3:
          vb = "danced with ";
          break;
        case 4:
          vb = "climbed up ";
          break;
        case 5:
          vb = "washed ";
          break;
        case 6:
          vb = "made ";
          break;
        case 7:
          vb = "wore ";
          break;
        case 8:
          vb = "watched ";
          break;
        case 9:
          vb = "jumped over ";
          break;
        case 0:
          vb = "ate ";
      }
      
      return vb;
      
    }
   public static String Objectt4 (int randomInt9){ //this will generate a random adjective from the ones that I listed
      String ob = "";
      switch (randomInt9){
        case 1:
          ob = "candy";
          break;
        case 2:
          ob = "bottle";
          break;
        case 3:
          ob = "keys";
          break;
        case 4:
          ob = "mop";
          break;
        case 5:
          ob = "apples";
          break;
        case 6:
          ob = "toothbrush";
          break;
        case 7:
          ob = "grass";
          break;
        case 8:
          ob = "scissors";
          break;
        case 9:
          ob = "moon";
          break;
        case 0:
          ob = "sweater";
      }
      
      return ob;
      
    }
}